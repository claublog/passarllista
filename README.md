# PassarLlista #

### What is it? ###

PassarLlista is an Android app that allows teachers to make attendance lists. The application permits to manage every student's absence on selected subjects that are in groups to facilitate the management. All the data is saved on a local data base.

### Download ###

You can download this app [here](http://www.mediafire.com/file/yufkebx77ngf7im).

### Design ###

![Diagrama.png](https://bitbucket.org/repo/6EXeA4/images/3312871021-Diagrama.png)